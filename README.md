A project done in my free time during the month of february, the idea behind this project is to make the two halfs of the brain work. Each side of one level is different for each entity.

Gameplay:  
![Gameplay](https://i.imgur.com/54w53B9.gif)  
Link for full quality: [https://streamable.com/9abnp](https://streamable.com/9abnp)  

The project was developed using Unity 2018.3.6 (tested to work till 2018.3.12).