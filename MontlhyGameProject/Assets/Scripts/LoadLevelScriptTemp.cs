﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadLevelScriptTemp : MonoBehaviour
{
    public void loadLevel(int buildIndex)
    {
        SceneManager.LoadScene(buildIndex);
    }
}
