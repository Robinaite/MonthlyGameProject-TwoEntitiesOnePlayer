﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField]
    private GameObject player1 = null;
    [SerializeField]
    private GameObject player2 = null;

    [SerializeField]
    private bool playerAlive = false;

    public float speed = 10.0f;
    [SerializeField]
    private float distanceToCheckColliders = 1f;

    private Vector3 movementVector = Vector3.zero;

    private bool up = false;
    private bool down = false;
    private bool left = false;
    private bool right = false;

    private bool movedToWall = false;

    private void Start()
    {
        if(player1 == null || player2 == null)
        {
            Debug.LogError("Players Objects not assigned to movement Manager.", this);
        }
    }


    public void Update()
    {
        if (playerAlive)
        {
            keyboardInput();
            movePlayer();
        }

    }

    public void setPlayerState(bool alive)
    {
        playerAlive = alive;
    }

    private void keyboardInput()
    {
        //Press Keys
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            up = true;
            down = false;
            right = false;
            left = false;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            up = false;
            down = true;
            right = false;
            left = false;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            up = false;
            down = false;
            right = false;
            left = true;
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            up = false;
            down = false;
            right = true;
            left = false;
        }
        //Release Key
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            up = false;
            checkPressedKeys();
        }
        else if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            down = false;
            checkPressedKeys();
        }
        else if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            left = false;
            checkPressedKeys();
        }
        else if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            right = false;
            checkPressedKeys();
        }

        //Debug.Log("Up-" + up + " Down-" + down+" Left-"+left+" Right-"+right);
    }

    private void checkPressedKeys()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            up = true;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            down = true;
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            left = true;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            right = true;
        }
    }

    private void movePlayer()
    {
        if (up)
        {
            movementVector = new Vector3(0, 0, 1);
        } else if (down)
        {
            movementVector = new Vector3(0, 0, -1);
        } else if (left)
        {
            movementVector = new Vector3(-1, 0, 0);
        } else if (right)
        {
            movementVector = new Vector3(1, 0, 0);
        } else
        {
            movementVector = Vector3.zero;
        }

        if (!playersColliding())
        {
            player1.transform.Translate(movementVector * speed * Time.deltaTime);
            player2.transform.Translate(movementVector * speed * Time.deltaTime);
        }
    }

    public bool playersColliding()
    {
        Rigidbody player1rigidBody = player1.GetComponent<Rigidbody>();

        //We need to check for the whole size of the player, so 2 raycasts are needed per player. 
        RaycastHit hit;

        Vector3 sideVecClockWise = new Vector3(movementVector.z, 0, -movementVector.x) * 0.48f;

        Ray player1Ray1 = new Ray(player1.transform.position + sideVecClockWise, movementVector);
        //Debug.DrawRay(player1.transform.position + sideVecClockWise, movementVector,Color.cyan);
        Ray player1Ray2 = new Ray(player1.transform.position - sideVecClockWise, movementVector);
        //Debug.DrawRay(player1.transform.position - sideVecClockWise, movementVector, Color.cyan);
        Ray player2Ray1 = new Ray(player2.transform.position + sideVecClockWise, movementVector);
        //Debug.DrawRay(player1.transform.position + sideVecClockWise, movementVector,Color.cyan);
        Ray player2Ray2 = new Ray(player2.transform.position - sideVecClockWise, movementVector);
        //Debug.DrawRay(player1.transform.position - sideVecClockWise, movementVector, Color.cyan);
        if (Physics.Raycast(player1Ray1,out hit, distanceToCheckColliders) || Physics.Raycast(player1Ray2, out hit, distanceToCheckColliders) 
            || Physics.Raycast(player2Ray1, out hit, distanceToCheckColliders) || Physics.Raycast(player2Ray2, out hit, distanceToCheckColliders))
        {
            if(hit.collider.tag == "NormalWall")
            {
                if (hit.distance > 0.5f)
                {
                    Vector3 testVec = movementVector.normalized * (hit.distance - 0.5f);
                    if (!movedToWall)
                    {
                        player1.transform.Translate(testVec);
                        player2.transform.Translate(testVec);
                        movedToWall = true;
                    }
                }
                else
                {
                    Vector3 testVec = movementVector.normalized * (hit.distance - 0.5f);
                    if (!movedToWall)
                    {
                        player1.transform.Translate(testVec);
                        player2.transform.Translate(testVec);
                        movedToWall = true;
                    }
                }
               return true;
            }
        }
        movedToWall = false;
        return false;
    }
}
