﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{

    public Transform playerToFollow;
    public float distanceToPlayer = 8f;


    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Camera>().orthographicSize = distanceToPlayer;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = playerToFollow.position + new Vector3(0, 4, 0);
    }
}
