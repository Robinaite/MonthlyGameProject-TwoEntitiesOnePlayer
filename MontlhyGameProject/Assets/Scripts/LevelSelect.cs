﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour
{
    private Canvas levelSelectCanvas = null;

    [SerializeField]
    private List<Level> levels = new List<Level>();
    [SerializeField]
    private GameObject buttonPrefab = null;
    [SerializeField]
    private GameObject buttonPlacementObject = null;


    private void Awake()
    {
        levelSelectCanvas = GetComponent<Canvas>();
    }

    private void Start()
    {
        if(levels.Count <= 0)
        {
            Debug.Log("No levels added to the levelSelect Menu.");
            return;
        }
        Level levelBefore = null;
        foreach (Level lvl in levels)
        {
            GameObject lvlButtonGO = Instantiate(buttonPrefab, buttonPlacementObject.transform);
            lvlButtonGO.GetComponentInChildren<Text>().text = lvl.level.ToString();
            Button lvlButton = lvlButtonGO.GetComponent<Button>();
            lvlButton.onClick.AddListener(() => loadLevel(lvl.level));
            if (levelBefore != null)
            {
                if (!levelBefore.isLevelCompleted())
                {
                    lvlButtonGO.GetComponent<Image>().enabled = false;
                    lvlButton.interactable = false;
                }
            }
            levelBefore = lvl;
        }
    }


    private void loadLevel(int levelID)
    {
        SceneManager.LoadScene(levelID);
    }
   





}
