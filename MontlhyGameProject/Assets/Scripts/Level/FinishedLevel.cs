﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishedLevel : MonoBehaviour
{

    [SerializeField]
    private LevelManager levelManager;

    [SerializeField]
    private bool executeLevelFinishScript = false;

    private void Start()
    {
        if (levelManager == null)
        {
            levelManager = FindObjectOfType<LevelManager>();
            Debug.LogWarning("Please add the levelManager To This Object in the inspector window.", this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && executeLevelFinishScript)
        {
            levelManager.finishedLevel();
        }
    }
}
