﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelManager : MonoBehaviour
{
    //Offset for the builderindex in unity. 
    public static int levelOffset = 1;
    private static int menuSceneBuildIndex = 0;

    [SerializeField]
    private PlayerMovement playerMovement = null;

    [SerializeField]
    private Level levelObject = null;

    [SerializeField]
    private Canvas retryCanvas = default;
    [SerializeField]
    private Canvas endLevelCanvas = default;

    [SerializeField]
    private List<collectCollectible> collectibles = new List<collectCollectible>();

    private void Awake()
    {
        if (levelObject == null)
        {
            Debug.LogError("No level Object selected for this scene.", this);
        }
        if (playerMovement == null)
        {
            Debug.LogError("No player movement Object selected for this scene.", this);
        }
    }

    void Start()
    {
        

        foreach (collectCollectible collectGameObject in collectibles)
        {
            collectGameObject.setLevelManager(this);
        }
    }

    public void collectedNewCollectible(Collectibles collectibleType)
    {
        levelObject.collectCollectible(collectibleType);
    }


    public void playerDied()
    {
        playerMovement.setPlayerState(false);
        retryCanvas.enabled = true;
    }

    public void resetLevel()
    {
        levelObject.resetScore();
        Debug.Log("Restarting Level!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void finishedLevel()
    {
        playerMovement.setPlayerState(false);
        //Show Finished Level UI
        endLevelCanvas.enabled = true;

        //Add collectibles to total.
        foreach(CollectiblePerLevelList c1 in levelObject.obtainCollectedCollectibles())
        {
            c1.collectible.IncreaseCollected(c1.obtainMissingCollectedAmount());
        }

        
    }

    public void nextLevel()
    {
        levelObject.resetScore();
        // Move to next level or main menu
        //Move to main menu
        if ((levelObject.level + levelOffset) >= SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(menuSceneBuildIndex);
        }
        else
        {
            SceneManager.LoadScene(levelObject.level + levelOffset);
        }
    }

}
