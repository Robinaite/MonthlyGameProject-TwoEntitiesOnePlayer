﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartLevel : MonoBehaviour
{
    [SerializeField]
    private LevelManager levelManager;

    private void Start()
    {
        if(levelManager == null)
        {
            levelManager = FindObjectOfType<LevelManager>();
            Debug.LogWarning("Please add the levelManager To This Object in the inspector window.",this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            levelManager.playerDied();
        }
    }

}
