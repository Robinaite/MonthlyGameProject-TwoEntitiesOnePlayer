﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Level 1", menuName = "Level", order = 1)]
public class Level : ScriptableObject
{
    public int level;
    [SerializeField]
    private bool levelCompleted = false;
    [SerializeField]
    private List<CollectiblePerLevelList> listCollectibleTypes = new List<CollectiblePerLevelList>();

    public void setLevelCompleted()
    {
        levelCompleted = true;
    }

    public bool isLevelCompleted()
    {
        return levelCompleted;
    }

    public void collectCollectible(Collectibles collectibleTypeToCompare)
    {
        foreach(CollectiblePerLevelList collectibleType in listCollectibleTypes)
        {
            if (collectibleTypeToCompare.obtainCollectibleName().Equals(collectibleType.collectible.obtainCollectibleName()))
            {
                collectibleType.increaseCollectedCollectible();
            }
        }
    }

    public List<CollectiblePerLevelList> obtainCollectedCollectibles()
    {
        return listCollectibleTypes;
    }


    public void resetScore()
    {
        foreach (CollectiblePerLevelList collectibleType in listCollectibleTypes)
        {
            collectibleType.resetCollected();
        }
    }


}
