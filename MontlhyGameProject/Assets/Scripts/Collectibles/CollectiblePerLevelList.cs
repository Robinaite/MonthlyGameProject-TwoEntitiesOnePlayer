﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CollectiblePerLevelList 
{
    public Collectibles collectible;
    [SerializeField]
    private int collected;
    [SerializeField]
    private int collectedAfterCompleted = 0;

    public void increaseCollectedCollectible()
    {
        collected++;
    }


    public void resetCollected()
    {
        collected = 0;
    }

    /// <summary>
    /// Returns the amount collected in the specific level if it's superior than what got collected before in the level.
    /// </summary>
    /// <returns></returns>
    public int obtainAllCollectedAmount()
    {
        return collected;
    }

    /// <summary>
    /// Returns the missing amount of collected collectibles in case the player collected more in one level than exists.
    /// </summary>
    /// <returns></returns>
    public int obtainMissingCollectedAmount()
    {
        if (collected > collectedAfterCompleted)
        {
            int missingAmount = collected - collectedAfterCompleted;
            collectedAfterCompleted = collected;
            return missingAmount;
        }
        return 0;
    }

}
