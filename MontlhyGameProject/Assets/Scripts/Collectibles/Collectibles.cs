﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Collectible", menuName = "Collectible", order = 1)]
public class Collectibles : ScriptableObject
{
    [SerializeField]
    private string collectibleName = default;

    [SerializeField]
    private int totalCollected = 0;

    public void IncreaseCollected(int amount)
    {
        totalCollected += amount;
    }

    public int currentAmount()
    {
        return totalCollected;
    }

    public string obtainCollectibleName()
    {
        return collectibleName;
    }
}
