﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class collectCollectible : MonoBehaviour
{
    [SerializeField]
    private Collectibles collectibleType = default;

    private LevelManager levelManager;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            notificateLevelManager();
            Destroy(this.gameObject);
        }
    }

    public void setLevelManager(LevelManager levelManagerToSet)
    {
        levelManager = levelManagerToSet;
    }

    private void notificateLevelManager()
    {
        levelManager.collectedNewCollectible(collectibleType);

    }

}
